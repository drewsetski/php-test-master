<?php

namespace OpsWay\Migration\Reader\File;

use OpsWay\Migration\Reader\ReaderInterface;

class Csv implements ReaderInterface
{

    protected $filename = '';

    protected $currentItem = 0;
    protected $products = [];

    public function __construct(array $params)
    {
        $this->filename = $params['filename'];
    }

    public function read()
    {
        $this->products = $this->csvToArray($this->filename);

        return $this->products[$this->currentItem++];
    }

    /**
     * Read csv file and transform it into array
     *
     * @param string $filename
     *
     * @return array|bool
     */
    function csvToArray($filename = '')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle)) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
}