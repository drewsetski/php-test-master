<?php

namespace OpsWay\Migration\Logger;

use OpsWay\Migration\Writer\WriterInterface;

class OutOfStockLogger
{

    protected $file;
    protected $filename;
    protected $debug;

    public function __construct(array $params, $mode = false)
    {
        $this->filename = $params['out_of_stock_file'];
        $this->debug = $mode;
        $this->checkFileName();
    }


    public function __invoke($item, $status, $msg)
    {
        if ($this->debug) {
            self::write($item);
        }
        if (!$status) {
            echo "Warning: " . $msg . print_r($item, true) . PHP_EOL;
        }
    }

    /**
     * @param $item array
     *
     * @return bool
     */
    public function write(array $item)
    {
        if (!$this->file) {
            if (!($this->file = fopen($this->filename, 'w+'))) {
                throw new \RuntimeException(sprintf('Can not create file "%s" for writing data.', $this->filename));
            }
            fputcsv($this->file, array_keys($item));
        }
        if ($item['qty'] == 0 && $item['is_stock'] == 0) {
            return fputcsv($this->file, $item);
        }
        return null;
    }

    public function __destruct()
    {
        if ($this->file) {
            fclose($this->file);
        }
    }

    private function checkFileName()
    {
        if (file_exists($this->filename)) {
            throw new \RuntimeException(sprintf('File "%s" already exists. Remove it and run again.', $this->filename));
        }
    }
}